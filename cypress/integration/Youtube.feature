Feature: Arriver sur la page d'accueil de YouTube

  Je veux ouvrir le site YouTube

  @focus
  Scenario: Ouvrir le site YouTube
    Given J'ouvre le site "https://www.youtube.com/"
    Then Je vois "YouTube" dans le titre
