import { Then } from "cypress-cucumber-preprocessor/steps";

Then(`Je vois {string} dans le titre`, (title) => {
    cy.title().should('include', title)
})
