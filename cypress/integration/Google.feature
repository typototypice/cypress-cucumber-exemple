Feature: Google Main Page

  I want to open a search engine

  @focus
  Scenario: Opening a search engine page
    Given I open Google page
    Then I see "Google" in the title

  @focus
  Scenario: Opening a search engine page Dragibus
    Given I open Google page
    Then I see "Dragibus" in the title
