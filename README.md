# Cypress Cucumber Exemple

## 🧐 A propos

Ce dépôt contient un exemple d'utilisation de Cypress avec Cucumber

## 🏁 Pour démarrer

```shell
npm install
npm run cypress:open
```

Une fois sur l'interface vous pouvez lancer des tests. Ces derniers vont générés des rapports cucumber au format json

Pour transformer ces rapports en rapport il faut utiliser la commande suivante :

```shell
npm run generate:report
```

Cette commande va générer un rapport au format html dans le dossier `cypress/cucumber/report`. Pour le visualiser vous pouvez ouvrir le fichier html dans un navigateur.

### Les tests exemples

Le projet contient 2 features :
- Une feature pour visiter le site Google avec un test passant et un test non passant
- Une feature pour vister le site Youtube

## 🔗 Liens utiles

- [cypress-cucumber-preprocessor](https://github.com/TheBrainFamily/cypress-cucumber-preprocessor)
- [multiple-cucumber-html-reporter](https://github.com/wswebcreation/multiple-cucumber-html-reporter)
